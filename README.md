# Fokus plasmoid

Fokus is simple pomodoro KDE Plasma plasmoid.

![](screenshots/1.png)

To install use:
plasmapkg2 -i ./package
